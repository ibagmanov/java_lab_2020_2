package ru.itis.javalab.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.services.AccountService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private AccountService accountService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        boolean flag = false;

        String uuidBySession = (String) request.getSession().getAttribute("auth");

        if (uuidBySession != null) {
            Account account = accountService.getAccountByUUID(uuidBySession);
            if (account != null)
                flag = true;
        }

        String myURL = request.getRequestURI();
        String loginURL = request.getContextPath() + "/login";
        String aurhURL = request.getContextPath() + "/home";

        if (myURL.equals(loginURL)) {
            if (flag) {
                response.sendRedirect("home");
                return false;
            }
            else {
                return true;
            }
        } else if (myURL.equals(aurhURL)) {
            if (flag)
                return true;
            else {
                response.sendRedirect("login");
                return false;
            }
        } else
            return true;
    }
}
