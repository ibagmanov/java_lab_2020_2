package ru.itis.javalab.services;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.repositories.AccountRepository;
import ru.itis.javalab.utils.EmailUtil;
import ru.itis.javalab.utils.MailsGenerator;

@Service
@Profile("master")
public class AccountServiceImpl implements AccountService {

    @Autowired
    private MailsGenerator mailsGenerator;

    @Autowired
    private EmailUtil emailUtil;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${server.url}")
    private String serverUrl;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean signIn(String login, String password) {
        Account account = accountRepository.findByLogin(login).orElse(null);
        if (account != null && passwordEncoder.matches(password, account.getPassword().trim())) {
            String confirmMail = mailsGenerator.getMailForConfirm(serverUrl, account.getUUID());
            emailUtil.sendMail(login, "Регистрация", from, confirmMail);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void delete(Account account) {
        accountRepository.delete(account.getId());
    }

    @Override
    public Account getAccountByLogin(String login) {
        return accountRepository.findByLogin(login).orElse(null);
    }

    @Override
    public Account getAccountByUUID(String uuid) {
        return accountRepository.findByUUID(uuid).orElse(null);
    }
}
