package ru.itis.javalab.utils;

import org.springframework.stereotype.Component;

@Component
public interface MailsGenerator {
    String getMailForConfirm(String serverUrl, String code);
}
