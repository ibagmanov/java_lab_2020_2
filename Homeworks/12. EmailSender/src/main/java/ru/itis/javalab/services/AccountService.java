package ru.itis.javalab.services;

import ru.itis.javalab.models.Account;

public interface AccountService {
    Account getAccountByLogin(String login);
    Account getAccountByUUID (String uuid);
    boolean signIn(String login, String password);
    void delete(Account account);
}
