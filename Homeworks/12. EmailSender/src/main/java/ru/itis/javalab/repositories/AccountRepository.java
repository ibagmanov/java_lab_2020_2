package ru.itis.javalab.repositories;

import ru.itis.javalab.models.Account;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Optional<Account> findByLogin(String login);
    Optional<Account> findByUUID (String uuid);
}
