package ru.itis.javalab.controllers;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.itis.javalab.services.AccountService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {

    @Autowired
    private AccountService accountService;


    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView doGet(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        String authCode = (String) request.getSession().getAttribute("auth");
        modelAndView.addObject("authCode", authCode);
        modelAndView.addObject("_csrf_token", request.getSession().getAttribute("_csrf_token"));

        return modelAndView;
    }

    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public ModelAndView doPost(HttpServletRequest request, HttpServletResponse response) {
        String uuid = (String) request.getSession().getAttribute("auth");
        if(request.getParameter("action") != null && request.getParameter("action").equals("delete")) {
            accountService.delete(accountService.getAccountByUUID(uuid));
        }
        return new ModelAndView("redirect:/logout");
    }
}
