
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Users</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div>
    <h1 style="
    color: ${cookieColor.getValue()}">USERS</h1>
    <form action="/users" method="post">
        <select name="color">
            <option value="red">RED</option>
            <option value="green">GREEN</option>
            <option value="blue">BLUE</option>
        </select>
        <input type="hidden" name="_csrf_token" value="${_csrf_token}">
        <input type="submit" value="OK">
    </form>
</div>
<div>
    <table>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
        </tr>

        <#list users as listUsers>
            <tr>
                <td>${listUsers.getFirstName()}</td>
                <td>${listUsers.getLastName()}</td>
                <td>${listUsers.getAge()}</td>
            </tr>
        </#list>
    </table>
</div>
</body>
</html>