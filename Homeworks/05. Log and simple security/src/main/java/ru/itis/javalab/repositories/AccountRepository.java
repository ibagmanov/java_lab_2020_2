package ru.itis.javalab.repositories;

import ru.itis.javalab.models.Account;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account> {
    Optional<Account> findByLogin(String login);
    Optional<Account> findByUUID (String UUID);
}
