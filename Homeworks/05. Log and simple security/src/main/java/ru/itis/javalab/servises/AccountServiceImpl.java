package ru.itis.javalab.servises;

import ru.itis.javalab.models.Account;
import ru.itis.javalab.repositories.AccountRepository;
import ru.itis.javalab.repositories.UserRepository;

public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccountByLogin(String login) {
        return accountRepository.findByLogin(login).orElse(null);
    }

    @Override
    public Account getAccountByUUID(String uuid) {
        return accountRepository.findByUUID(uuid).orElse(null);
    }
}
