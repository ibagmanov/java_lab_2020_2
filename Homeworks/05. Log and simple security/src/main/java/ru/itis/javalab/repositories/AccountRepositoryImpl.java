package ru.itis.javalab.repositories;

import ru.itis.javalab.models.Account;
import ru.itis.javalab.models.User;

import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_LOGIN = "select * from account where login = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_UUID = "select * from account where uuid = ?";

    private DataSource dataSource;
    private SimpleJdbcTemplate template;

    public AccountRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
        this.template = new SimpleJdbcTemplate(dataSource);
    }

    private RowMapper<Account> accountRowMapper = row -> Account.builder()
            .id(row.getInt("id"))
            .login(row.getString("login"))
            .password(row.getString("password"))
            .UUID(row.getString("uuid"))
            .build();

    @Override
    public Optional<Account> findByLogin(String login) {
        Account account = null;
        List<Account> list = template.query(SQL_SELECT_BY_LOGIN, accountRowMapper, login);
        if(list.size() != 0)
            account = list.get(0);
        return Optional.ofNullable(account);
    }

    @Override
    public Optional<Account> findByUUID(String UUID) {
        Account account = null;
        List<Account> list = template.query(SQL_SELECT_BY_UUID, accountRowMapper, UUID);
        if(list.size() != 0)
            account = list.get(0);
        return Optional.ofNullable(account);
    }

    @Override
    public List<Account> findAll() {
        return null;
    }

    @Override
    public Optional<Account> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(Account entity) {

    }

    @Override
    public void update(Account entity) {

    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void delete(Account entity) {

    }
}
