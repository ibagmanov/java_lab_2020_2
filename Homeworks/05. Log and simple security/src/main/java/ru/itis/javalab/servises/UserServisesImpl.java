package ru.itis.javalab.servises;

import ru.itis.javalab.models.User;
import ru.itis.javalab.repositories.UserRepository;

import java.util.List;

public class UserServisesImpl implements UserService {

    private UserRepository userRepository;

    public UserServisesImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
