package ru.itis.javalab.servises;

import ru.itis.javalab.models.Account;

public interface AccountService {
    Account getAccountByLogin(String login);
    Account getAccountByUUID (String uuid);
}
