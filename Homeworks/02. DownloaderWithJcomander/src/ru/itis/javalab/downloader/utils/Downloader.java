package ru.itis.javalab.downloader.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.net.URL;
import ru.itis.javalab.downloader.app.Args;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Downloader {
    public static void downloader(Args args) {

        boolean mulit = false;
        ExecutorService service = null;

        switch (args.mode) {
            case "multi-thread":
                service = Executors.newFixedThreadPool(args.count);
                mulit = true;
                break;
            case "one-thread":
                break;
            default:
                throw new IllegalArgumentException("Mode's not true");
        }

        List<String> files = args.files;

        if (!Files.exists(Path.of(args.folder))) {
            new File(args.folder).mkdirs();
            if (args.folder.charAt(args.folder.length() - 1) != '/')
                args.folder += "/";
        }
        while (!files.isEmpty()) {
            Runnable task = () -> {
                try {
                    URL url;
                    synchronized (files) {
                        if (!files.isEmpty())
                            url = new URL(files.remove(0));
                        else
                            return;
                    }
                    String[] path = url.getFile().split("/");
                    String name = args.folder + path[path.length - 1];
                    System.out.println("Downloading: " + name);
                    Files.copy(url.openStream(), new File(name).toPath());
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            };
            if (mulit) {
                service.submit(task);
            } else {
                task.run();
            }
        }
        if (service != null)
            service.shutdown();
    }
}
