INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (1, 'admin', 'admin', '79d9b198-5515-4fd0-beea-3acf7b8de0b4', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (2, 'ildar', 'bagmanov', '4a59ef22-5233-4650-a3ca-832bab694069', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (3, 'bulat', 'tuhvatullin', '25047ab5-7d6a-46d7-81be-34cb544757c9', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (4, 'kek', 'kek', '3b51bf69-9000-4328-b089-e1f59461f9e8', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (8, 'qwerty007BC', '$2a$10$vAdOEgTJhreb5frnk.6P8O9Vttdu8jGA7HI5Qnf/GaKfY3OAQjBt6', '162ecefc-3cbe-4d0d-9fca-842e3f7ea37c', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (9, 'qwerty008BC', '$2a$10$D3cF6a1gik4PkdHlLgdVZOyqMTN30XJnTPljDa5OaKaPlzP6u5zQe', '162ecefc-3cbe-4d0d-9fca-842e3f7ea37d', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (5, 'yaUstal', 'yaToshe', '162ecefc-3cbe-4d0d-9fca-842e3f7ea37a', false);
INSERT INTO public.account (id, login, password, uuid, is_deleted) VALUES (7, 'adminBC', '$2a$10$bhuVxOjkaUxViY.588qclu1yAcwC8jNPq2hg4n.TXL53mV3JKfUa.', '162ecefc-3cbe-4d0d-9fca-842e3f7ea37b', false);