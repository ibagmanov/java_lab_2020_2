-- Table: public.account

-- DROP TABLE public.account;

CREATE TABLE public.account
(
    id integer NOT NULL DEFAULT nextval('account_id_seq'::regclass),
    login character varying(150) COLLATE pg_catalog."default" NOT NULL,
    password character varying(300) COLLATE pg_catalog."default" NOT NULL,
    uuid character varying(36) COLLATE pg_catalog."default" NOT NULL,
    is_deleted boolean DEFAULT false,
    CONSTRAINT account_pk PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.account
    OWNER to postgres;
-- Index: account_uuid_uindex

-- DROP INDEX public.account_uuid_uindex;

CREATE UNIQUE INDEX account_uuid_uindex
    ON public.account USING btree
    (uuid COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;