package ru.itis.javalab.annotationprocessor.futureForms;

import ru.itis.javalab.annotationprocessor.annotations.HtmlForm;
import ru.itis.javalab.annotationprocessor.annotations.HtmlInput;

@HtmlForm(method = "post", action = "/users")
public class WorkBook {
    @HtmlInput(name = "name", placeholder = "Название тетради")
    private String name;
    @HtmlInput(name = "cost",type = "text", placeholder = "Стоимость")
    private Integer cost;
    @HtmlInput(name = "color", type = "text", placeholder = "Цвет")
    private String color;
}
