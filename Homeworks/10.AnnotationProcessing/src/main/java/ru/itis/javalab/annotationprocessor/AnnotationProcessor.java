package ru.itis.javalab.annotationprocessor;

import com.google.auto.service.AutoService;
import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.javalab.annotationprocessor.annotations.HtmlForm;
import ru.itis.javalab.annotationprocessor.annotations.HtmlInput;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * 03.12.2020
 * 15.Annotations_SOURCE
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@AutoService(Processor.class)
@SupportedAnnotationTypes(value = {"ru.itis.javalab.annotationprocessor.annotations.HtmlForm"})
public class AnnotationProcessor extends AbstractProcessor {
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {

        Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(HtmlForm.class);

        for (Element element : annotatedElements) {

            List<InputData> informations = AnnotationProcessor.giveInputData(element.getEnclosedElements());

            try {
                Configuration configuration = new Configuration(Configuration.VERSION_2_3_30);
                configuration.setDefaultEncoding("UTF-8");
                configuration.setTemplateLoader(new FileTemplateLoader(new File("src/main/resources/")));
                Template template = configuration.getTemplate("form.ftlh");

                String path = AnnotationProcessor.class.getProtectionDomain().getCodeSource().getLocation().getPath();
                path = path.substring(1) + element.getSimpleName().toString() + ".html";
                Path out = Paths.get(path);

                Map<String, Object> attributes = new HashMap<>();

                HtmlForm annotation = element.getAnnotation(HtmlForm.class);
                Map.Entry<String, String> pairMethodAction = new AbstractMap.SimpleEntry<>(annotation.method(),annotation.action());

                attributes.put("formData", pairMethodAction);
                attributes.put("inputData", informations);

                BufferedWriter writer = new BufferedWriter(new FileWriter(out.toFile()));
                template.process(attributes, writer);

                writer.close();

            } catch (IOException | TemplateException e) {
                throw new IllegalStateException(e);
            }

        }
        return true;
    }

    private static List<InputData> giveInputData(List<? extends Element> elements){
        List<InputData> informations = new ArrayList<>();

        for(Element element : elements){
            HtmlInput htmlInput = element.getAnnotation(HtmlInput.class);
            if(htmlInput != null) {
                informations.add(new InputData(htmlInput.type(), htmlInput.name(), htmlInput.placeholder()));
            }
        }
        return informations;
    }
}
