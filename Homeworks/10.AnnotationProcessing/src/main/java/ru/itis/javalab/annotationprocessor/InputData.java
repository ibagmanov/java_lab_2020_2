package ru.itis.javalab.annotationprocessor;

public class InputData {
    private String type;
    private String name;
    private String placeholder;

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public InputData(String type, String name, String placeholder) {
        this.type = type;
        this.name = name;
        this.placeholder = placeholder;
    }
}
