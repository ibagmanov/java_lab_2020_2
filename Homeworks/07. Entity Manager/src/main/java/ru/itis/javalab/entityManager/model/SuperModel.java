package ru.itis.javalab.entityManager.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class SuperModel {
    private boolean boolean1;
    private Boolean boolean2;
    private double numeric;
    private Double numeric2;
    private float real;
    private Float real2;
    private Integer integer;
    private int integer2;
    private BigDecimal numeric3;
    private Date date;
    private Time time;
    private Timestamp timestamp;
    private Long bigint;
    private long bigint2;
    private String varchar;
    private short smallint;

    public SuperModel() {
    }

    @Override
    public String toString() {
        return "SuperModel{" +
                "boolean1=" + boolean1 +
                ", boolean2=" + boolean2 +
                ", numeric=" + numeric +
                ", numeric2=" + numeric2 +
                ", real=" + real +
                ", real2=" + real2 +
                ", integer=" + integer +
                ", integer2=" + integer2 +
                ", numeric3=" + numeric3 +
                ", date=" + date +
                ", time=" + time +
                ", timestamp=" + timestamp +
                ", bigint=" + bigint +
                ", bigint2=" + bigint2 +
                ", varchar='" + varchar + '\'' +
                ", smallint=" + smallint +
                '}';
    }

    public SuperModel(boolean boolean1, Boolean boolean2, double numeric, Double numeric2, float real, Float real2, Integer integer, int integer2, BigDecimal numeric3, Date date, Time time, Timestamp timestamp, Long bigint, long bigint2, String varchar, short smallint) {
        this.boolean1 = boolean1;
        this.boolean2 = boolean2;
        this.numeric = numeric;
        this.numeric2 = numeric2;
        this.real = real;
        this.real2 = real2;
        this.integer = integer;
        this.integer2 = integer2;
        this.numeric3 = numeric3;
        this.date = date;
        this.time = time;
        this.timestamp = timestamp;
        this.bigint = bigint;
        this.bigint2 = bigint2;
        this.varchar = varchar;
        this.smallint = smallint;
    }

    public boolean isBoolean1() {
        return boolean1;
    }

    public Boolean getBoolean2() {
        return boolean2;
    }

    public double getNumeric() {
        return numeric;
    }

    public Double getNumeric2() {
        return numeric2;
    }

    public float getReal() {
        return real;
    }

    public Float getReal2() {
        return real2;
    }

    public Integer getInteger() {
        return integer;
    }

    public int getInteger2() {
        return integer2;
    }

    public BigDecimal getNumeric3() {
        return numeric3;
    }

    public Date getDate() {
        return date;
    }

    public Time getTime() {
        return time;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public Long getBigint() {
        return bigint;
    }

    public long getBigint2() {
        return bigint2;
    }

    public String getVarchar() {
        return varchar;
    }

    public short getSmallint() {
        return smallint;
    }
}
