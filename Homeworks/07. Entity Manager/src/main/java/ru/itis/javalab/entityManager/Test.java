package ru.itis.javalab.entityManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.itis.javalab.entityManager.model.SuperModel;
import ru.itis.javalab.entityManager.model.User;

import javax.sql.DataSource;
import java.lang.reflect.Field;
import java.sql.Types;

public class Test {
    public static void main(String[] args) throws Exception {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/javalab_hwDB_v2");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty808");
        hikariConfig.setMaximumPoolSize(20);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        EntityManager entityManager = new EntityManager(dataSource);
        entityManager.createTable("testTable", User.class);
        entityManager.save("testTable", new User(1L, "Ildar", "Bagmanov", true));
        entityManager.save("testTable", new User(2L, "Dinar", "Bagmanov", true));
        entityManager.save("testTable", new User(3L, "Bulat", "Bagmanov", true));

        System.out.println(entityManager.findById("testTable", User.class, Long.class, 2L));

        /*entityManager.createTable("superModelTable", SuperModel.class);
        entityManager.save("superModelTable", new SuperModel());*/
    }
}
