package ru.itis.javalab.entityManager;


import javax.sql.DataSource;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class EntityManager {
    private DataSource dataSource;

    public EntityManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> void createTable(String tableName, Class<T> entityClass) {
        StringBuilder sqlQuery = new StringBuilder("create table \"" + tableName + "\" (");
        Field[] fields = entityClass.getDeclaredFields();

        for (Field field : fields) {
            String sqlType = EntityManagerUtils.javaTypeToJDBCType(field.getType().getSimpleName());
            if (sqlType == null) {
                throw new IllegalStateException("Exception: [JavaType " + field.getType().getName() + " -> to SQL = ?]");
            }
            sqlQuery.append("\"").append(field.getName()).append("\" ").append(sqlType).append(",");
        }
        sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));
        sqlQuery.append(");");

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery.toString())) {
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public void save(String tableName, Object entity) {
        Class<?> entityClass = entity.getClass();

        StringBuilder sqlQuery = new StringBuilder("insert into \"" + tableName + "\" (");
        StringBuilder sqlValues = new StringBuilder(" VALUES (");

        Field[] fields = entityClass.getDeclaredFields();
        for (Field field : fields) {
            sqlQuery.append("\"").append(field.getName()).append("\",");
            sqlValues.append("?,");
        }
        sqlQuery.deleteCharAt(sqlQuery.lastIndexOf(","));
        sqlValues.deleteCharAt(sqlValues.lastIndexOf(","));
        sqlQuery.append(")").append(sqlValues).append(");");

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(String.valueOf(sqlQuery))) {
                int parameter = 1;
                for (Field field : fields) {
                    field.setAccessible(true);
                    preparedStatement.setObject(parameter, field.get(entity));
                    parameter++;
                }
                preparedStatement.executeUpdate();
            }
        } catch (SQLException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }

    public <T, ID> T findById(String tableName, Class<T> resultType, Class<ID> idType, ID idValue) {
        //language=SQL
        String sqlQuery = "SELECT * from \"" + tableName + "\" where id = ? limit 1;";

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery)) {
                preparedStatement.setObject(1, idValue);
                ResultSet resultSet = preparedStatement.executeQuery();

                if (resultSet.next()) {
                    Class<?>[] classes = new Class[resultType.getDeclaredFields().length];
                    Object[] objects = new Object[resultType.getDeclaredFields().length];
                    Field[] fields = resultType.getDeclaredFields();
                    for (int i = 0; i < fields.length; i++) {
                        classes[i] = fields[i].getType();
                        objects[i] = resultSet.getObject(fields[i].getName().toLowerCase());
                    }

                    Constructor<T> constructor = resultType.getConstructor(classes);
                    return constructor.newInstance(objects);
                } else {
                    return null;
                }
            }
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | SQLException | NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }
}

class EntityManagerUtils {

    private static Map<String, String> types;

    static {
        types = new HashMap<>();
        types.put("boolean", "boolean");
        types.put("Boolean", "boolean");
        types.put("double", "numeric");
        types.put("Double", "numeric");
        types.put("float", "real");
        types.put("Float", "real");
        types.put("Integer", "integer");
        types.put("int", "integer");
        types.put("BigDecimal", "numeric");
        types.put("Date", "date");
        types.put("Time", "time");
        types.put("Timestamp", "timestamp");
        types.put("Long", "bigint");
        types.put("long", "bigint");
        types.put("String", "varchar(255)");
        types.put("short", "smallint");
    }

    public static String javaTypeToJDBCType(String type) {
        return types.get(type);
    }
}
