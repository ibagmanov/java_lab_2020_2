package ru.itis.javalab.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServerSocket {

    public void start(int port) {
        ServerSocket server;

        try {
            server = new ServerSocket(port);

            while (true) {
                Socket client = server.accept();

                BufferedReader fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                PrintWriter toClient = new PrintWriter(client.getOutputStream(), true);

                ClientHandler clientHandler = new ClientHandler(client, fromClient, toClient);
                new Thread(clientHandler.getTask()).start();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

}
