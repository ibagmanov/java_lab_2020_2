package ru.itis.javalab.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ClientHandler {

    private static Map<Integer, ClientHandler> clientChats = new HashMap<>();
    private static int idGenerator = 1;

    private int id;
    private Socket client;
    private BufferedReader reader;
    private PrintWriter writer;

    public ClientHandler(Socket client, BufferedReader reader, PrintWriter writer) {
        this.client = client;
        this.reader = reader;
        this.writer = writer;
        this.id = idGenerator;
        idGenerator++;
        clientChats.put(this.id, this);
    }

    public Runnable getTask() {
        return task;
    }

    private Runnable task = () -> {

        try {
            writer.println("____________Hello, Guest#" + this.id + ". It is the best chat!!!____________");
            sendMsgToAll("I'm join to chat!");
            String input = reader.readLine();
            while(input != null) {
                sendMsgToAll(input);
                input = reader.readLine();
            }

        } catch (IOException e) {
            System.err.println("Guest#" + this.id + ": Exit");
        }
    };

    private void sendMsgToAll(String message) {
        clientChats.forEach((clientId, clientHandler) -> {
            try{
                if (clientId != this.id) {
                    clientHandler.writer.println("Guest#" + this.id + ": "+ message);
                }

            } catch (Exception e) {
                clientChats.remove(clientId);
                try {
                    clientHandler.writer.close();
                    clientHandler.reader.close();
                    clientHandler.client.close();
                } catch (IOException ex) {
                    throw new IllegalStateException(ex);
                }
            }
        });
    }
}
