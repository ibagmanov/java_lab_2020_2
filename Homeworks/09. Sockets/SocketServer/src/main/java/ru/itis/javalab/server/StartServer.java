package ru.itis.javalab.server;

import com.beust.jcommander.JCommander;

public class StartServer {
    public static void main(String[] argv) {
        Args args = new Args();
        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);

        EchoServerSocket serverSocket = new EchoServerSocket();
        serverSocket.start(args.port);
    }
}
