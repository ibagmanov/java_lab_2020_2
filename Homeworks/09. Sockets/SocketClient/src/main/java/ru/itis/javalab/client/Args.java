package ru.itis.javalab.client;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Args {

    @Parameter(names = "--server-ip", required = true)
    public String serverIp;

    @Parameter(names = "--server-port", required = true)
    public Integer serverPort;

}
