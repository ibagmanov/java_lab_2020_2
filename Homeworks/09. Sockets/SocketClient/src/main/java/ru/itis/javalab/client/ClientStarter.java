package ru.itis.javalab.client;

import com.beust.jcommander.JCommander;

import java.util.Scanner;

public class ClientStarter {
    public static void main(String[] argv) {
        Args args = new Args();
        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);

        ClientSocket clientSocket = new ClientSocket(args.serverIp, args.serverPort);
        Scanner sc = new Scanner(System.in);

        while(true) {
            clientSocket.sendMessage(sc.nextLine());
        }
    }
}
