package ru.itis.javalab.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute("auth", "");
        Cookie csrfCookieDeleter = new Cookie("_csrf_token", "");
        csrfCookieDeleter.setMaxAge(0);
        resp.addCookie(csrfCookieDeleter);

        resp.sendRedirect("login");
    }
}
