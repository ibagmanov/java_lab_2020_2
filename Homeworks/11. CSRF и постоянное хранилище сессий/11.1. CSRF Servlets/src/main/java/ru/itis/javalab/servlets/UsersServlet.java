package ru.itis.javalab.servlets;

import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.context.ApplicationContext;
import ru.itis.javalab.models.User;
import ru.itis.javalab.services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private UserService userService;
    private Configuration configuration;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("applicationContext");
        this.userService = applicationContext.getBean(UserService.class);

        this.configuration = applicationContext.getBean(Configuration.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> users = userService.getAllUsers();
        Cookie cookieColor = getColorCookie(request.getCookies());

        Map<String, Object> attributes = new HashMap<>();
        attributes.put("users", users);
        attributes.put("cookieColor", cookieColor);
        attributes.put("_csrf_token", request.getSession().getAttribute("_csrf_token"));
        Template template = this.configuration.getTemplate("users.ftl");

        response.setCharacterEncoding("UTF-8");

        try {
            template.process(attributes, response.getWriter());
        } catch (TemplateException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String color = req.getParameter("color");
        Cookie cookie = new Cookie("color", color);
        cookie.setMaxAge(60 * 60 * 24 * 365);
        resp.addCookie(cookie);
        resp.sendRedirect("/users");
    }

    private Cookie getColorCookie(Cookie[] cookies) {
        Cookie cookieColor = null;
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals("color")) {
                cookieColor = cookie;
                break;
            }
        }
        if(cookieColor == null)
            cookieColor = new Cookie("color", "red");
        return cookieColor;
    }

}
