package ru.itis.javalab.filters;

import org.springframework.context.ApplicationContext;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.services.AccountService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter implements Filter {

    private ApplicationContext applicationContext;
    private AccountService accountService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        this.applicationContext = (ApplicationContext) servletContext.getAttribute("applicationContext");
        this.accountService = this.applicationContext.getBean(AccountService.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletResponse.setCharacterEncoding("UTF-8");
        servletRequest.setCharacterEncoding("UTF-8");

        boolean flag = false;

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String uuidBySession = (String) request.getSession().getAttribute("auth");

        if (uuidBySession != null) {
            Account account = accountService.getAccountByUUID(uuidBySession);
            if (account != null)
                flag = true;
        }

        String myURL = request.getRequestURI();
        String loginURL = request.getContextPath() + "/login";
        String aurhURL = request.getContextPath() + "/home";

        if (myURL.equals(loginURL)) {
            if (flag)
                ((HttpServletResponse) servletResponse).sendRedirect("home");
            else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else if (myURL.equals(aurhURL)) {
            if (flag)
                filterChain.doFilter(servletRequest, servletResponse);
            else {
                ((HttpServletResponse) servletResponse).sendRedirect("login");
            }
        } else
            filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
