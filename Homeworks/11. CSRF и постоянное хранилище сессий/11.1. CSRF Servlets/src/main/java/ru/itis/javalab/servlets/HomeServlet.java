package ru.itis.javalab.servlets;

import freemarker.cache.WebappTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.context.ApplicationContext;
import ru.itis.javalab.services.AccountService;
import ru.itis.javalab.services.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {

    private AccountService accountService;
    private Configuration configuration;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("applicationContext");
        this.accountService = applicationContext.getBean(AccountService.class);
        this.configuration = applicationContext.getBean(Configuration.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Template template = configuration.getTemplate("home.ftl");

        String authCode = (String) req.getSession().getAttribute("auth");
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("authCode", authCode);
        attributes.put("_csrf_token", req.getSession().getAttribute("_csrf_token"));

        try {
            template.process(attributes, resp.getWriter());
        } catch (TemplateException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = (String) req.getSession().getAttribute("auth");
        if(req.getParameter("action") != null && req.getParameter("action").equals("delete")) {
            accountService.delete(accountService.getAccountByUUID(uuid));
        }
        resp.sendRedirect("/logout");
    }
}
