<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>
</head>
<body style="text-align: center; padding-left: 45%; font-size: 20px">

<form method="post" action="login" style="width: 100px">
    <input type="hidden" name="_csrf_token" value="${_csrf_token}">
    <label for="login">Login</label>
    <input name="login" id="login" type="text" maxlength="20" required>

    <label for="password">Password</label>
    <input name="password" id="password" type="password" maxlength="20" required>

    <button type="submit">Sing in</button>
</form>

</body>
</html>