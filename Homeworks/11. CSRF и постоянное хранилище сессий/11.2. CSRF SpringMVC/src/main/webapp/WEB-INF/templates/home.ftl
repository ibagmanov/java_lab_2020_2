<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title>home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>

<h1>Secret cookie is: ${authCode}</h1>
<h2>Secret csrf-token is: ${_csrf_token}</h2>
<button><a href="logout">Удалить куки</a></button>

<form action="/home?action=delete" method="post">
    <input type="hidden" name="_csrf_token" value="${_csrf_token}">
    <input type="submit" value="Delete">
</form>
</body>
</html>