package ru.itis.javalab.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.repositories.AccountRepository;

@Service(value = "accountService")
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    private PasswordEncoder passwordEncoder;

    public AccountServiceImpl(AccountRepository accountRepository, PasswordEncoder passwordEncoder) {
        this.accountRepository = accountRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public boolean signIn(String login, String password) {
        Account account = accountRepository.findByLogin(login).orElse(null);
        if (account != null && passwordEncoder.matches(password, account.getPassword().trim()))
            return true;
        else
            return false;
    }

    @Override
    public void delete(Account account) {
        accountRepository.delete(account.getId());
    }

    @Override
    public Account getAccountByLogin(String login) {
        return accountRepository.findByLogin(login).orElse(null);
    }

    @Override
    public Account getAccountByUUID(String uuid) {
        return accountRepository.findByUUID(uuid).orElse(null);
    }
}
