package ru.itis.javalab.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.itis.javalab.models.Account;

import java.util.*;

@Repository(value = "accountRepository")
public class AccountRepositoryJdbcTemplateImpl implements AccountRepository {

    //language=SQL
    private static final String SQL_FIND_ALL = "select  * from account";
    //language=SQL
    private static final String SQL_FIND_ONE_BY_ID = "select * from account where id = :id limit 1";
    //language=SQL
    private static final String SQL_FIND_ONE_BY_LOGIN = "select * from account where login = :login limit 1";
    //language=SQL
    private static final String SQL_FIND_ONE_BY_UUID = "select * from account where uuid = :uuid limit 1";
    //language=SQL
    private static final String SQL_INSERT = "insert into account(login, password, uuid, is_deleted)" +
            "value (:login, :password, :uuid, :is_deleted)";
    //language=SQL
    private static final String SQL_UPDATE = "update account set login=:login, password=:password, uuid=:uuid, is_deleted=:is_deleted where id = :id;";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public AccountRepositoryJdbcTemplateImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private RowMapper<Account> accountRowMapper = (row, rowNumber) -> Account.builder()
            .id(row.getLong("id"))
            .login(row.getString("login"))
            .password(row.getString("password"))
            .UUID(row.getString("uuid"))
            .isDeleted(row.getBoolean("is_deleted"))
            .build();

    @Override
    public Optional<Account> findByLogin(String login) {
        try {
            //Collections - секрет успеха сеньора.
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_FIND_ONE_BY_LOGIN, Collections.singletonMap("login", login), accountRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Account> findByUUID(String uuid) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_FIND_ONE_BY_UUID, Collections.singletonMap("uuid", uuid), accountRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Account entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("login", entity.getLogin());
        params.put("password", entity.getPassword());
        params.put("uuid", entity.getUUID());
        params.put("is_deleted", entity.getIsDeleted());

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource(params);
        KeyHolder key = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(SQL_INSERT, sqlParameterSource, key);
        entity.setId(Objects.requireNonNull(key.getKey()).longValue());
    }

    @Override
    public void delete(Long aLong) {
        Account account =  findById(aLong).orElse(null);
        if(account == null) return;
        account.setIsDeleted(true);
        update(account);
    }

    @Override
    public void update(Account entity) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put("id", entity.getId());
        attributes.put("uuid", entity.getUUID());
        attributes.put("password", entity.getPassword());
        attributes.put("login", entity.getLogin());
        attributes.put("is_deleted", entity.getIsDeleted());
        namedParameterJdbcTemplate.update(SQL_UPDATE, attributes);
     }

    @Override
    public Optional<Account> findById(Long aLong) {
        try {
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_FIND_ONE_BY_ID, Collections.singletonMap("id", aLong), accountRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Account> findAll() {
        return null;
    }
}
