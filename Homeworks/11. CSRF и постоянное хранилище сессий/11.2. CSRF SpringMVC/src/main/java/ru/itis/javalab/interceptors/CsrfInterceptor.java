package ru.itis.javalab.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Component
public class CsrfInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getMethod().equals("POST")) {
            String requestCsrf = request.getParameter("_csrf_token");
            String sessionCsrf = (String) request.getSession(false).getAttribute("_csrf_token");
            if (sessionCsrf.equals(requestCsrf)) {
                return true;
            } else {
                response.sendRedirect("home");
                return false;
            }
        }

        boolean cookieIsLive = false;
        if (request.getMethod().equals("GET")) {
            for(Cookie cookie : request.getCookies()) {
                if(cookie.getName().equals("_csrf_token")) {
                    request.getSession().setAttribute("_csrf_token", cookie.getValue());
                    cookieIsLive = true;
                    break;
                }
            }
            if(!cookieIsLive){
                Cookie csrfTokenCookie = new Cookie("_csrf_token", UUID.randomUUID().toString());
                csrfTokenCookie.setMaxAge(60*60*24);
                response.addCookie(csrfTokenCookie);
            }
        }
        return true;
    }
}
