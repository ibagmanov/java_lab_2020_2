package ru.itis.javalab.controllers;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.itis.javalab.models.User;
import ru.itis.javalab.services.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UsersController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView doGet(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("users");
        modelAndView.addObject("users", userService.getAllUsers());
        modelAndView.addObject("cookieColor", getColorCookie(request.getCookies()));
        modelAndView.addObject("_csrf_token", request.getSession().getAttribute("_csrf_token"));
        return modelAndView;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ModelAndView doPost(HttpServletRequest req, HttpServletResponse resp) {
        String color = req.getParameter("color");
        Cookie cookie = new Cookie("color", color);
        cookie.setMaxAge(60 * 60 * 24 * 365);
        resp.addCookie(cookie);
        return new ModelAndView("redirect:/users");
    }

    private Cookie getColorCookie(Cookie[] cookies) {
        Cookie cookieColor = null;
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals("color")) {
                cookieColor = cookie;
                break;
            }
        }
        if(cookieColor == null)
            cookieColor = new Cookie("color", "red");
        return cookieColor;
    }
}
