package java.ru.itis.javalab.threadpool;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ThreadPool threadPool = ThreadPool.newPool(3);

        Runnable task1 = () -> {
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " A = " + i);
            }
        };

        Runnable task2 = () -> {
            for (int i = 0; i < 20; i++) {
                System.out.println(Thread.currentThread().getName() + " B = " + i);
            }
        };

        Runnable task3 = () -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " C = " + i);
            }
        };

        Scanner sc = new Scanner(System.in);

        String input = sc.nextLine();
        while (!input.equals("STOP")) {
            String[] strings = input.split(";");
            for (String string : strings) {
                switch (string) {
                    case "task1":
                        threadPool.submit(task1);
                        break;
                    case "task2":
                        threadPool.submit(task2);
                        break;
                    case "task3":
                        threadPool.submit(task3);
                        break;
                    default:
                        System.out.println("ERROR: " + string);
                }
            }
            input = sc.nextLine();
        }
        threadPool.stop();

    }
}
