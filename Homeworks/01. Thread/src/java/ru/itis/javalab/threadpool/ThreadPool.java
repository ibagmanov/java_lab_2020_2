package java.ru.itis.javalab.threadpool;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

public class ThreadPool {
    private Deque<Runnable> tasks;

    private PoolWorker[] pool;

    boolean isWork = true;

    public static ThreadPool newPool(int threadsCount) {
        ThreadPool threadPool = new ThreadPool();
        threadPool.tasks = new ConcurrentLinkedDeque<>();
        threadPool.pool = new PoolWorker[threadsCount];

        for (int i = 0; i < threadPool.pool.length; i++) {
            threadPool.pool[i] = threadPool.new PoolWorker();
            threadPool.pool[i].start();
        }
        return threadPool;
    }

    public void submit(Runnable task) {
        tasks.add(task);
        synchronized (tasks) {
            tasks.notifyAll();
        }
    }

    public void stop() {
        isWork = false;
        synchronized (tasks) {
            tasks.notifyAll();
        }
    }

    private class PoolWorker extends Thread {

        @Override
        public void run() {
            Runnable task;
            while (true) {
                synchronized (tasks) {
                    if (tasks.isEmpty()) {
                        try {
                            tasks.wait();
                            if(!isWork)
                                break;
                        } catch (InterruptedException e) {
                            throw new IllegalArgumentException(e);
                        }
                    }
                    task = tasks.pollFirst();
                }
                if (task != null)
                    task.run();
            }
        }
    }


}
