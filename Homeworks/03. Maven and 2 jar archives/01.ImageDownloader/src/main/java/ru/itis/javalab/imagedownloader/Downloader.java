package ru.itis.javalab.imagedownloader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Downloader {

    public static void multiDownloader(int count, List<String> files, String folder) {
        downloader(true, count, files, folder);
    }

    public static void oneDownloader(List<String> files, String folder) {
        downloader(false, 0, files, folder);
    }

    private static void downloader(boolean multiMode, int count, List<String> files, String folder) {

        boolean mulit = false;
        ExecutorService service = null;

        if (multiMode) {
            service = Executors.newFixedThreadPool(count);
            mulit = true;
        }

        if (!Files.exists(Path.of(folder))) {
            new File(folder).mkdirs();
            if (folder.charAt(folder.length() - 1) != '/')
                folder += "/";
        }
        while (!files.isEmpty()) {
            String finalFolder = folder;
            Runnable task = () -> {
                try {
                    URL url;
                    synchronized (files) {
                        if (!files.isEmpty())
                            url = new URL(files.remove(0));
                        else
                            return;
                    }
                    String[] path = url.getFile().split("/");
                    String name = finalFolder + path[path.length - 1];
                    System.out.println("Downloading: " + name);
                    Files.copy(url.openStream(), new File(name).toPath());
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            };
            if (mulit) {
                service.submit(task);
            } else {
                task.run();
            }
        }
        if (service != null)
            service.shutdown();
    }
}
