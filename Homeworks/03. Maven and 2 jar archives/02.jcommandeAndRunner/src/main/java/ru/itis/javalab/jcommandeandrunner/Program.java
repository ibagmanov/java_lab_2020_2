package ru.itis.javalab.jcommandeandrunner;

import com.beust.jcommander.JCommander;
import ru.itis.javalab.imagedownloader.Downloader;

public class Program {

    public static void main(String[] argv) {

        Args args = new Args();

        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);

        System.out.println("Mode:= " + args.mode);
        System.out.println("Count:= " + args.count);
        System.out.println("Folder:= " + args.folder);
        System.out.println("Array:= " + args.files.toString() + "\n" + "Array's size:= " + args.files.size());
        System.out.println("---------------------------------------------------");

        switch (args.mode){
            case "one-thread":
                Downloader.oneDownloader(args.files, args.folder);
                break;
            case "multi-thread":
                Downloader.multiDownloader(args.count, args.files, args.folder);
                break;
            default:
                System.out.println("Error in mode!");
        }
    }

}
