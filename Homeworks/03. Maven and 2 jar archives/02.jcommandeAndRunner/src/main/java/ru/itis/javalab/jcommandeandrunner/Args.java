package ru.itis.javalab.jcommandeandrunner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.*;

@Parameters(separators = "=")
public class Args {

    @Parameter(names = "--mode", required = true)
    public String mode = "";

    @Parameter(names = "--count")
    public int count = 1;

    @Parameter(names = "--files", required = true, listConverter = FileListConverter.class)
    public List<String> files;

    @Parameter(names = "--folder")
    public String folder = "download";

    public static class FileListConverter implements IStringConverter<List<String>> {
        @Override
        public List<String> convert(String files) {
            String [] paths = files.split(",");
            return new LinkedList<>(Arrays.asList(paths));
        }
    }

}



// --mode=multi-thread --count=10 --files=URL,URL --folder=C:/images