package ru.itis.javalab.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javalab.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account getAccountById(Long Id);
}
