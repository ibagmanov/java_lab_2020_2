package ru.itis.javalab.service;

import ru.itis.javalab.dto.RegisterDto;
import ru.itis.javalab.model.Account;

public interface AccountService {
    boolean signUp(Account account);
}
