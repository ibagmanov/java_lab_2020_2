package ru.itis.javalab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.itis.javalab.dto.RegisterDto;
import ru.itis.javalab.model.Account;
import ru.itis.javalab.service.AccountService;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class SignUpController {

    @Autowired
    AccountService accountService;

    @GetMapping(value = "/signup")
    public String doGet(Model model) {
        model.addAttribute("registerDto", new RegisterDto());
        return "register";
    }

    @PostMapping(value = "/signup")
    public String doPost(@Valid RegisterDto dto, BindingResult bindingResult, Model model) {
        if (!bindingResult.hasErrors()){
            Account account = Account.builder()
                    .login(dto.getLogin())
                    .email(dto.getEmail())
                    .password(dto.getPassword())
                    .city(dto.getCity())
                    .name(dto.getName())
                    .surname(dto.getSurname())
                    .uuid(UUID.randomUUID().toString())
                    .build();
            if(accountService.signUp(account)){
                return "redirect:/account";
            }
        }
        model.addAttribute("registerDto", dto);
        return "register";

    }
}
