package ru.itis.javalab.dto;

import lombok.Data;

@Data
public class AccountDto {
    private String login;
    private String password;
    private String email;
}
