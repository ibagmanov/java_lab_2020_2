package ru.itis.javalab.dto;

import lombok.Data;
import ru.itis.javalab.validation.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class RegisterDto {
    private String login;
    @NotEmpty(message = "{errors.empty.email}")
    @Email(message = "{errors.incorrect.email}")
    private String email;
    @ValidPassword(message = "{errors.invalid.password}")
    private String password;
    private String city;
    private String name;
    private String surname;
}
