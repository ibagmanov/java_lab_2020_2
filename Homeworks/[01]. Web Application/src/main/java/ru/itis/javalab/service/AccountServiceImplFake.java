package ru.itis.javalab.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.itis.javalab.model.Account;
import ru.itis.javalab.repository.AccountRepository;
import ru.itis.javalab.util.EmailUtil;
import ru.itis.javalab.util.MailsGenerator;

@Service
@Profile("dev")
public class AccountServiceImplFake implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private MailsGenerator mailsGenerator;

    @Autowired
    private EmailUtil emailUtil;

    @Value("${server.url}")
    private String serverUrl;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean signUp(Account account) {
        try {
            accountRepository.save(account);
            String confirmMail = mailsGenerator.getMailForConfirm(serverUrl, account.getUuid());
            System.out.println(confirmMail);
            System.out.println("_______FAKE______");
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }
}

