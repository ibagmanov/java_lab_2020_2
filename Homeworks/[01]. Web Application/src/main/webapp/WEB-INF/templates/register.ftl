<#ftl encoding="UTF-8"/>
<!DOCTYPE html>
<html lang="ru">
<#import "spring.ftl" as spring/>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Register</title>

    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body style="text-align: center; padding-left: 45%; font-size: 20px">
<h2><a href="?lang=ru">RU</a> </h2>
<h2><a href="?lang=en">EN</a> </h2>
<@spring.bind "registerDto"/>
<form method="post" action="signup" style="width: 100px">
    <label for="login"><@spring.message 'sign_up_page.registration.login'/></label>
    <input name="login" id="login" type="text" required>

    <label for="email"><@spring.message 'sign_up_page.registration.email'/></label>
    <@spring.formInput "registerDto.email"/>
    <@spring.showErrors "<br>" "error"/>
    <br>
    <label for="password"><@spring.message 'sign_up_page.registration.password'/></label>
    <@spring.formPasswordInput "registerDto.password"/>
    <@spring.showErrors "<br>" "error"/>
    <br>
    <label for="name"><@spring.message 'sign_up_page.registration.name'/></label>
    <input name="name" id="name" type="text" required>

    <label for="surname"><@spring.message 'sign_up_page.registration.surname'/></label>
    <input name="surname" id="surname" type="text" required>

    <label for="city"><@spring.message 'sign_up_page.registration.city'/></label>
    <input name="city" id="city" type="text" required>

    <button type="submit"><@spring.message 'sign_up_page.registration.signup'/></button>
</form>

</body>
</html>
