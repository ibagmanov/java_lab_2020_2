package ru.itis.javalab.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.itis.javalab.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

public class UserRepositoryJdbcImpl implements UserRepository {
    //language=SQL
    private static final String SQL_SELECT_BY_AGE = "select * from \"user\" where age = :age";
    //language=SQL
    private static final String SQL_SELECT = "select * from \"user\"";
    //language=SQL
    private static final String SQL_FIND_BY_FIRST_AND_SECOND_NAMES = "select * from \"user\" where first_name = :firstName AND last_name = :lastName limit 1";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserRepositoryJdbcImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private RowMapper<User> userRowMapper = (row, rowLine) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .age(row.getInt("age"))
            .build();

    @Override
    public List<User> findAllByAge(Integer age) {
        Map<String, Object> parms = new HashMap<>();
        parms.put("age", age);
        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_AGE, parms, userRowMapper);
    }

    @Override
    public Optional<User> findFirstByFirstnameAndLastname(String firstName, String lastName) {
        try {
            Map<String, Object> parms = new HashMap<>();
            parms.put("firstName", firstName);
            parms.put("lastName", lastName);
            return Optional.of(namedParameterJdbcTemplate.queryForObject(SQL_FIND_BY_FIRST_AND_SECOND_NAMES, parms, userRowMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public List<User> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT, userRowMapper);
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void save(User entity) {

    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(Long aLong) {

    }
}
