package ru.itis.javalab.servises;

import ru.itis.javalab.models.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
}
