package ru.itis.javalab.filters;

import ru.itis.javalab.models.Account;
import ru.itis.javalab.servises.AccountService;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"})
public class AuthFilter implements Filter {

    private AccountService accountService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ServletContext servletContext = filterConfig.getServletContext();
        accountService = (AccountService) servletContext.getAttribute("accountService");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        boolean flag = false;

        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String uuidBySession = (String) request.getSession().getAttribute("auth");
        if(uuidBySession != null) {
            Account account = accountService.getAccountByUUID(uuidBySession);
            if (account != null)
                flag = true;
        }

        String myURL = request.getRequestURI();
        String loginURL = request.getContextPath() + "/login";
        String aurhURL = request.getContextPath() + "/home";
        System.out.println(myURL + "\n" + loginURL);

        if (myURL.equals(loginURL)) {
            if (flag)
                ((HttpServletResponse) servletResponse).sendRedirect("home");
            else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } else if (myURL.equals(aurhURL)) {
            if (flag)
                filterChain.doFilter(servletRequest, servletResponse);
            else {
                ((HttpServletResponse) servletResponse).sendRedirect("login");
            }
        } else
            filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
