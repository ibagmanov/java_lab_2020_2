package ru.itis.javalab.servlets;

import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.servises.AccountService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private AccountService accountService;
    private PasswordEncoder passwordEncoder;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        accountService = (AccountService) servletContext.getAttribute("accountService");
        passwordEncoder = (PasswordEncoder) servletContext.getAttribute("passwordEncoder");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/WEB-INF/views/login.html").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //TODO: BCrypt добавить и работа с сессией (СДЕЛАНО)
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        //String passwordHash = passwordEncoder.encode(password);
        Account account = accountService.getAccountByLogin(login);
        if (account != null && passwordEncoder.matches(password, account.getPassword().trim())) {
            req.getSession().setAttribute("auth", account.getUUID());
            resp.sendRedirect("home");
        } else {
            resp.sendRedirect("login");
        }
    }
}
