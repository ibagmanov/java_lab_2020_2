web.xml - дескриптор развертывания

Чтобы запустить томкат через консоль, нужно в папке томкад/bin запустить консоль и прописать
./startup.bat - запускает этот файл

Дальше добавим менеджера.
В папке томкад/conf открываем tomcat-users.xml

<?xml version="1.0" encoding="UTF-8"?>
<tomcat-users xmlns="http://tomcat.apache.org/xml"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://tomcat.apache.org/xml tomcat-users.xsd"
              version="1.0">
  <role rolename="manager-gui"/>
  <user username="tomcat" password="tomcat" roles="tomcat, manager-gui"/>
</tomcat-users>

Дальше по url: localhost:8080/manager можем попасть в панель администратора

Чтобы выключить томкат через консоль нужен: ./shutdown.bat

В папке томкад/webapp находятся все проекты.

Проекты, которые запускаются с корня (Example: localhost:8080/тут ничего нет/helloServlet) находятся в папке ROOT.

Чтобы работать с БД нужно в папку томкад/lib загрузить JDBC бд.
