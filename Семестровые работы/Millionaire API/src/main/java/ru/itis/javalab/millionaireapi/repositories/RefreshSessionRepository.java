package ru.itis.javalab.millionaireapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javalab.millionaireapi.models.RefreshSessions;

import java.util.Optional;

public interface RefreshSessionRepository extends JpaRepository<RefreshSessions, Long> {

    Optional<RefreshSessions> getByFingerprintAndUserIdAndRefreshToken(String fingerprint, Long userId, String refreshToken);

    Optional<RefreshSessions> getByFingerprintAndUserId(String fingerprint, Long userId);
}
