package ru.itis.javalab.millionaireapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.javalab.millionaireapi.models.MethodsLogger;

import java.util.Optional;

public interface MethodsLoggerRepository extends JpaRepository<MethodsLogger, Long> {
    Optional<MethodsLogger> getMethodsLoggerByMethodNameAndUrlAndMethodTypeAndClassName(String methodName, String url, String methodType, String className);
}
