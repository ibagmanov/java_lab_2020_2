package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.millionaireapi.service.AccountService;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/account/{account-id}/block")
    public ResponseEntity<?> blockAccount(@PathVariable("account-id") Long accountId){
        accountService.blockAccount(accountId);
        return ResponseEntity.ok("");
    }
}
