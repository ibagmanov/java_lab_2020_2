package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.millionaireapi.dto.JwtAndRefreshDto;
import ru.itis.javalab.millionaireapi.dto.LoginForm;
import ru.itis.javalab.millionaireapi.service.LoginService;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;



    @PostMapping("/login")
    public ResponseEntity<JwtAndRefreshDto> authenticate(@RequestBody LoginForm loginForm) {

        return ResponseEntity.ok(loginService.login(loginForm));
    }

}
