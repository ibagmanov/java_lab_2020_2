package ru.itis.javalab.millionaireapi.redis.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javalab.millionaireapi.models.Account;
import ru.itis.javalab.millionaireapi.redis.models.RedisUser;
import ru.itis.javalab.millionaireapi.redis.repository.RedisUsersRepository;
import ru.itis.javalab.millionaireapi.repositories.AccountRepository;
import ru.itis.javalab.millionaireapi.service.JwtBlacklistService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RedisAccountServiceImpl implements RedisAccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private JwtBlacklistService blacklistService;

    @Autowired
    private RedisUsersRepository redisUsersRepository;

    @Override
    public void addTokenToAccount(Account account, String token) {
        String redisId = account.getRedisId();

        RedisUser redisUser;
        if (redisId != null) {
            redisUser = redisUsersRepository.findById(redisId).orElseThrow(IllegalArgumentException::new);
            if (redisUser.getTokens() == null) {
                redisUser.setTokens(new ArrayList<>());
            }
            redisUser.getTokens().add(token);
        } else {
            redisUser = RedisUser.builder()
                    .userId(account.getId())
                    .tokens(Collections.singletonList(token))
                    .build();
        }
        redisUsersRepository.save(redisUser);
        account.setRedisId(redisUser.getId());
        accountRepository.save(account);
    }

    @Override
    public void addAllTokensToBlackList(Account account) {
        if (account.getRedisId() != null) {
            RedisUser redisUser = redisUsersRepository.findById(account.getRedisId())
                    .orElseThrow(IllegalArgumentException::new);

            List<String> tokens = redisUser.getTokens();
            for (String token : tokens) {
                blacklistService.add(token);
            }
            redisUser.getTokens().clear();
            redisUsersRepository.save(redisUser);
        }
    }

    @Override
    public void addTokenToBlackList(Account account, String token) {
        if (account.getRedisId() != null) {
            RedisUser redisUser = redisUsersRepository.findById(account.getRedisId())
                    .orElseThrow(IllegalArgumentException::new);

            List<String> tokens = redisUser.getTokens();
            for (int i = 0; i < tokens.size(); i++) {
                if (tokens.get(i).equals(token)) {
                    blacklistService.add(token);
                    tokens.remove(i);
                    break;
                }
            }
            redisUsersRepository.save(redisUser);
        }
    }
}
