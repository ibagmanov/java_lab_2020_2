package ru.itis.javalab.millionaireapi.service;

import ru.itis.javalab.millionaireapi.dto.TokenDto;
import ru.itis.javalab.millionaireapi.models.RefreshSessions;

public interface RefreshSessionService {

    RefreshSessions save(RefreshSessions save);
    RefreshSessions getByFingerprintAndUserIdAndRefreshToken(String fingerprint, Long userId, String refreshToken);
    RefreshSessions getByFingerprintAndUserId(String fingerprint, Long userId);
    RefreshSessions createNewRefreshSession(Long userId, String fingerprint);
    RefreshSessions updateToken(String jwt, String refreshToken);
}
