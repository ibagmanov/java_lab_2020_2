package ru.itis.javalab.millionaireapi.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javalab.millionaireapi.models.RefreshSessions;
import ru.itis.javalab.millionaireapi.redis.services.RedisAccountService;
import ru.itis.javalab.millionaireapi.repositories.AccountRepository;
import ru.itis.javalab.millionaireapi.repositories.RefreshSessionRepository;

import java.util.UUID;

@Service
public class RefreshSessionServiceImpl implements RefreshSessionService {

    @Autowired
    private RefreshSessionRepository repository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RedisAccountService redisAccountService;

    @Override
    public RefreshSessions save(RefreshSessions save) {
        return repository.save(save);
    }

    @Override
    public RefreshSessions getByFingerprintAndUserIdAndRefreshToken(String fingerprint, Long userId, String refreshToken) {
        return repository.getByFingerprintAndUserIdAndRefreshToken(fingerprint, userId, refreshToken).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public RefreshSessions getByFingerprintAndUserId(String fingerprint, Long userId) {
        return repository.getByFingerprintAndUserId(fingerprint, userId).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    public RefreshSessions createNewRefreshSession(Long userId, String fingerprint) {
        RefreshSessions session = repository.getByFingerprintAndUserId(fingerprint, userId).orElse(null);

        if (session == null) {
            session = RefreshSessions.builder()
                    .userId(userId)
                    .fingerprint(fingerprint)
                    .refreshToken(UUID.randomUUID().toString())
                    .build();
        } else {
            session.setRefreshToken(UUID.randomUUID().toString());
        }
        return repository.save(session);
    }

    @Override
    public RefreshSessions updateToken(String jwt, String refreshToken) {
        DecodedJWT decodedJwt = JWT.decode(jwt);
        String fingerprint = decodedJwt.getSubject();
        Long accountId = Long.valueOf(fingerprint);

        RefreshSessions oldSession = repository.getByFingerprintAndUserIdAndRefreshToken(fingerprint, accountId, refreshToken)
                .orElseThrow(IllegalArgumentException::new);

        redisAccountService.addTokenToBlackList(accountRepository.getAccountById(accountId), jwt);
        oldSession.setRefreshToken(UUID.randomUUID().toString());
        return repository.save(oldSession);
    }
}
