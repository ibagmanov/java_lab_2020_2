package ru.itis.javalab.millionaireapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.javalab.millionaireapi.models.Account;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RegistrationForm {

    private String email;
    private String password;
    private String username;

    public static RegistrationForm form (Account account) {
        return RegistrationForm.builder()
                .email(account.getEmail())
                .password(account.getPassword())
                .username(account.getUsername())
                .build();
    }
    public static List<RegistrationForm> form (List<Account> accounts) {
        return accounts.stream().map(RegistrationForm::form).collect(Collectors.toList());
    }

}
