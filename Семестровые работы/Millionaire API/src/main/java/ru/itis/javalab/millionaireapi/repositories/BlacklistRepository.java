package ru.itis.javalab.millionaireapi.repositories;

public interface BlacklistRepository {
    void save(String token);

    boolean exists(String token);
}
