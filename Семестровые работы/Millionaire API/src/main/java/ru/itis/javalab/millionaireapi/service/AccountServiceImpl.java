package ru.itis.javalab.millionaireapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javalab.millionaireapi.models.Account;
import ru.itis.javalab.millionaireapi.redis.repository.RedisUsersRepository;
import ru.itis.javalab.millionaireapi.redis.services.RedisAccountService;
import ru.itis.javalab.millionaireapi.repositories.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RedisAccountService redisAccountService;

    @Override
    public void blockAccount(Long accountId) {
        Account account = accountRepository.getAccountById(accountId);
        redisAccountService.addAllTokensToBlackList(account);
    }
}
