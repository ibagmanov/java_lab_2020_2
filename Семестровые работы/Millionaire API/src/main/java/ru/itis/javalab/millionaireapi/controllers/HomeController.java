package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    @GetMapping("/home")
    public ResponseEntity<?> getHome() {
        return ResponseEntity.ok(Collections.singletonMap("Title", "it is page home"));
    }
}
