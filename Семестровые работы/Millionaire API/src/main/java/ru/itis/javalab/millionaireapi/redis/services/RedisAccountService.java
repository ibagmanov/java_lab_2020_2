package ru.itis.javalab.millionaireapi.redis.services;


import ru.itis.javalab.millionaireapi.models.Account;

public interface RedisAccountService {
    void addTokenToAccount(Account account, String token);

    void addAllTokensToBlackList(Account account);

    void addTokenToBlackList(Account account, String token);
}
