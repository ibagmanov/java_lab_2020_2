package ru.itis.javalab.millionaireapi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.itis.javalab.millionaireapi.models.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Account getAccountById(Long Id);
    Optional<Account> findByEmail(String email);
    Optional<Account> getAccountByEmail(String email);
}
