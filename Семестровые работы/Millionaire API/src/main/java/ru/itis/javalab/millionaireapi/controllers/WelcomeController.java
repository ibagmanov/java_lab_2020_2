package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
public class WelcomeController {

    @GetMapping("/welcome")
    public ResponseEntity<?> getWelcome() {
        return ResponseEntity.ok(Collections.singletonMap("Title", "you are guest now"));
    }
}
