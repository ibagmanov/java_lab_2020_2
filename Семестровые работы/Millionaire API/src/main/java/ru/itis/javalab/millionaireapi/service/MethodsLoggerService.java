package ru.itis.javalab.millionaireapi.service;

import ru.itis.javalab.millionaireapi.models.MethodsLogger;

import java.util.Optional;

public interface MethodsLoggerService {
    MethodsLogger save(MethodsLogger methodsLogger);
    MethodsLogger getMethod (String methodName, String url, String methodType, String className);
}
