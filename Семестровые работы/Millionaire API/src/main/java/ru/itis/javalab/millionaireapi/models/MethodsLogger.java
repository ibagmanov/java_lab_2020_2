package ru.itis.javalab.millionaireapi.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class MethodsLogger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String className;
    String url;
    String methodType;
    String methodName;

    Long count;
}
