package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.millionaireapi.dto.RegistrationForm;
import ru.itis.javalab.millionaireapi.models.Account;
import ru.itis.javalab.millionaireapi.service.LoginService;

@RestController
public class RegistrationController {

    @Autowired
    LoginService loginService;


    @PostMapping("/registration")
    public ResponseEntity<Account> register(@RequestBody RegistrationForm information) {
        return ResponseEntity.ok(loginService.registration(information));
    }
}
