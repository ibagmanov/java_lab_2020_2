package ru.itis.javalab.millionaireapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JwtAndRefreshDto {

    private String token;
    private String refreshToken;
}
