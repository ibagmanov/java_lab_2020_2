package ru.itis.javalab.millionaireapi.service;

public interface AccountService {

    void blockAccount(Long accountId);
}
