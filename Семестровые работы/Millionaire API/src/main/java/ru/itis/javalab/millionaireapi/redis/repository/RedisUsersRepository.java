package ru.itis.javalab.millionaireapi.redis.repository;


import org.springframework.data.keyvalue.repository.KeyValueRepository;
import ru.itis.javalab.millionaireapi.redis.models.RedisUser;

public interface RedisUsersRepository extends KeyValueRepository<RedisUser, String> {
}
