package ru.itis.javalab.millionaireapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.javalab.millionaireapi.models.MethodsLogger;
import ru.itis.javalab.millionaireapi.repositories.MethodsLoggerRepository;

import java.util.Optional;

@Service
public class MethodsLoggerServiceImpl implements MethodsLoggerService {

    @Autowired
    private MethodsLoggerRepository repository;

    @Override
    public MethodsLogger save(MethodsLogger methodsLogger) {
        return repository.save(methodsLogger);
    }

    @Override
    public MethodsLogger getMethod(String methodName, String url, String methodType, String className) {
        return repository.getMethodsLoggerByMethodNameAndUrlAndMethodTypeAndClassName(methodName, url, methodType, className).orElse(null);
    }
}
