package ru.itis.javalab.millionaireapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.javalab.millionaireapi.dto.JwtAndRefreshDto;
import ru.itis.javalab.millionaireapi.dto.TokenDto;
import ru.itis.javalab.millionaireapi.models.RefreshSessions;
import ru.itis.javalab.millionaireapi.service.LoginService;
import ru.itis.javalab.millionaireapi.service.RefreshSessionService;

@RestController
public class RefreshTokenController {

    @Autowired
    private LoginService loginService;

    @Autowired
    private RefreshSessionService refreshSessionService;

    @PostMapping("/refresh_token")
    public ResponseEntity<JwtAndRefreshDto> refreshToken(@RequestBody JwtAndRefreshDto dto) {
        RefreshSessions sessions = refreshSessionService.updateToken(dto.getToken(), dto.getRefreshToken());
        TokenDto token = loginService.updateJwt(dto.getToken());
        return ResponseEntity.ok(JwtAndRefreshDto.builder().token(token.getToken()).refreshToken(sessions.getRefreshToken()).build());
    }
}
