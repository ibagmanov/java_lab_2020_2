package ru.itis.javalab.millionaireapi.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import ru.itis.javalab.millionaireapi.models.MethodsLogger;
import ru.itis.javalab.millionaireapi.service.MethodsLoggerService;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class BetaAspect {

    @Autowired
    private MethodsLoggerService methodsLoggerService;

    @Before(value = "execution(* ru.itis.javalab.millionaireapi.controllers.*.*(..))")
    public void addMethodToDB(JoinPoint joinPoint) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String methodName = joinPoint.getSignature().getName();
        String methodType = request.getMethod();
        String url = request.getRequestURI();
        String[] classPath = joinPoint.getSignature().getDeclaringType().getName().split("\\.");
        String className = classPath[classPath.length - 1];

        MethodsLogger logger = methodsLoggerService.getMethod(methodName, url, methodType, className);
        if (logger == null) {
            logger = MethodsLogger.builder()
                    .methodName(methodName)
                    .methodType(methodType)
                    .url(url)
                    .className(className)
                    .count(0L)
                    .build();
        }
        logger.setCount(logger.getCount() + 1L);
        methodsLoggerService.save(logger);
    }
}
