package ru.itis.javalab.millionaireapi.service;

import ru.itis.javalab.millionaireapi.dto.JwtAndRefreshDto;
import ru.itis.javalab.millionaireapi.dto.LoginForm;
import ru.itis.javalab.millionaireapi.dto.RegistrationForm;
import ru.itis.javalab.millionaireapi.dto.TokenDto;
import ru.itis.javalab.millionaireapi.models.Account;

public interface LoginService {
    Account registration(RegistrationForm form);

    JwtAndRefreshDto login(LoginForm loginForm);

    TokenDto updateJwt(String token);
}
