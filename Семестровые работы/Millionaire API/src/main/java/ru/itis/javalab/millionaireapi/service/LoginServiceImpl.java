package ru.itis.javalab.millionaireapi.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.millionaireapi.dto.JwtAndRefreshDto;
import ru.itis.javalab.millionaireapi.dto.LoginForm;
import ru.itis.javalab.millionaireapi.dto.RegistrationForm;
import ru.itis.javalab.millionaireapi.dto.TokenDto;
import ru.itis.javalab.millionaireapi.models.Account;
import ru.itis.javalab.millionaireapi.models.RefreshSessions;
import ru.itis.javalab.millionaireapi.redis.services.RedisAccountService;
import ru.itis.javalab.millionaireapi.repositories.AccountRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;
import java.util.function.Supplier;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private Algorithm algorithm;

    @Autowired
    private RefreshSessionService refreshService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RedisAccountService redisAccountService;

    @Override
    public Account registration(RegistrationForm form) {
        Account account = Account.builder()
                .username(form.getUsername())
                .email(form.getEmail())
                .password(passwordEncoder.encode(form.getPassword()))
                .role(Account.Role.ROLE_USER)
                .state(Account.State.ACTIVE)
                .uuid(UUID.randomUUID().toString())
                .build();
        accountRepository.save(account);
        return account;
    }

    @Override
    public JwtAndRefreshDto login(LoginForm loginForm) {
        Account account = accountRepository.getAccountByEmail(loginForm.getEmail())
                .orElseThrow(IllegalArgumentException::new);
        if (passwordEncoder.matches(loginForm.getPassword(), account.getPassword())) {
            String token = jwtGenerator(account);
            redisAccountService.addTokenToAccount(account, token);

            RefreshSessions sessions = refreshService.createNewRefreshSession(account.getId(), account.getId().toString());

            return JwtAndRefreshDto.builder().token(token).refreshToken(sessions.getRefreshToken()).build();
        } else {
            throw new UsernameNotFoundException("Invalid username or password");
        }
    }

    @Override
    public TokenDto updateJwt(String token) {
        Account account = accountRepository.getAccountById(Long.valueOf(JWT.decode(token).getSubject()));
        return TokenDto.builder().token(jwtGenerator(account)).build();
    }

    private String jwtGenerator(Account account) {
        return JWT.create()
                .withSubject(account.getId().toString())
                .withClaim("role", account.getRole().toString())
                .withClaim("state", account.getState().toString())
                .withClaim("email", account.getEmail())
                .withClaim("createdAt", new Date().getTime())
                .sign(algorithm);
    }
}
