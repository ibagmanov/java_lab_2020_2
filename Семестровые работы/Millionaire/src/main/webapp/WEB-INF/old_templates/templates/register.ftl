<#ftl encoding="UTF-8"/>
<!DOCTYPE html>
<html lang="ru">
<#import "spring.ftl" as spring/>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Регистрация</title>

    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body style="text-align: center; padding-left: 45%; font-size: 20px">
<h2><a href="?lang=ru">RU</a> </h2>
<h2><a href="?lang=en">EN</a> </h2>
<@spring.bind "registrationFormDto"/>
<form method="post" action="register" style="width: 100px">
    <label for="username"><@spring.message 'register.username'/></label>
    <input name="username" id="username" type="text" required>

    <label for="email"><@spring.message 'register.email'/></label>
    <@spring.formInput "registrationFormDto.email"/>
    <@spring.showErrors "<br>" "error"/>
    <br>
    <label for="password"><@spring.message 'register.password'/></label>
    <@spring.formPasswordInput "registrationFormDto.password"/>
    <@spring.showErrors "<br>" "error"/>
    <br>


    <button type="submit"><@spring.message 'register.register.button'/></button>
</form>

</body>
</html>
