<#ftl encoding="UTF-8"/>
<!doctype html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <link href="/css/home.css" rel="stylesheet">
    <title>Home (Profile)</title>
</head>
<body>


<nav class="navbar justify-content-between">
    <a id="navbar_brand" class="navbar-brand">Millionaire</a>
    <form class="form-inline">
        <a href="/logout"><button id="navbar_btn2" class="btn btn-success my-2 my-sm-0" type="button">Выйти</button></a>
    </form>
</nav>

<div class="container">
    <div id="profile_bio" class="row">
        <div id="photo_block" class="col-2">
            <img id="profile_photo" src="https://narodna-pravda.ua/wp-content/uploads/2018/03/miljoner-bagatij.jpg">
        </div>
        <div id="text" class="col-8">
            <p>Username: Ildar</p>
            <p>Email: ildarbagmanov7@gmail.com</p>
            <p>Money: 30 000 000$</p>
            <button class="earnButton" type="submit">10$</button>
            <button class="earnButton" type="submit">20$</button>
            <button class="earnButton" type="submit">30$</button>
            <button class="earnButton" type="submit">40$</button>
            <button class="earnButton" type="submit">Earn random money</button>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
