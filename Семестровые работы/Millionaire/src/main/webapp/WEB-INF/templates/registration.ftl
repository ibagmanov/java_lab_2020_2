<#ftl encoding="UTF-8"/>
<!DOCTYPE html>
<html lang="en">
<#import "spring.ftl" as spring/>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="/css/registration.css" rel="stylesheet">
    <title>Registration</title>
    <style>
        .error {
            color: red;
        }
    </style>

</head>
<body>

<h2><a href="?lang=ru">RU</a> </h2>
<h2><a href="?lang=en">EN</a> </h2>
<@spring.bind "registrationFormDto"/>

<div class="container">
    <div class="row">
        <div id="from_block">
            <form method="post" action="/registration">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                <div class="form-group">
                    <label for="username"><@spring.message 'register.username'/></label>
                    <input name="username" id="username" type="text" required>
                </div>
                <div class="form-group">
                    <label for="email"><@spring.message 'register.email'/></label>
                    <@spring.formInput "registrationFormDto.email"/>
                    <@spring.showErrors "<br>" "error"/>
                </div>
                <div class="form-group">
                    <label for="password"><@spring.message 'register.password'/></label>
                    <@spring.formPasswordInput "registrationFormDto.password"/>
                    <@spring.showErrors "<br>" "error"/>
                </div>
<#--                <div class="form-check">-->
<#--                    <input type="checkbox" class="form-check-input" id="remember_me">-->
<#--                    <label class="form-check-label" for="remember_me">Remember me</label>-->
<#--                </div>-->
                <button id="button" type="submit" class="btn btn-primary"><@spring.message 'register.register.button'/></button>
            </form>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
