<#ftl encoding="UTF-8"/>
<!doctype html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <link href="/css/welcome.css" rel="stylesheet">
    <title>Welcome to Millionaire</title>
</head>
<body>


<nav class="navbar justify-content-between">
    <a id="navbar_brand" class="navbar-brand">Millionaire</a>
    <form class="form-inline">
        <a href="/login"><button id="navbar_btn1" class="btn btn-success my-2 my-sm-0" type="button">Войти</button></a>
        <a href="/registration"><button id="navbar_btn2" class="btn btn-success my-2 my-sm-0" type="button">Зарегистрироваться</button></a>

    </form>
</nav>

<div id="main_con" class="container">
    <div class="row" style="text-align: center">
        <div class="col-6">
            <p id="description">Добро пожаловать<br>на сайт бизнесменов</p>
            <h1 id="title">“Millionaire”</h1>
            <a href="/login"><button id="start_earn" type="button" class="btn btn-success">Начать зарабатывать</button></a>

        </div>
        <div class="col-6">
            <img id="main_money" src="/img/money.png" alt="Money">
        </div>

    </div>
</div>

<div id="top_millionaire_block" class="container-fluid">
    <div class="row">
        <div class="col-6">
            <img id="money2_img" src="/img/money2.png" alt="Money">
        </div>
        <div id="top_users" class="col-5">
            <h2 id="top_title">Лучшие игроки</h2>
            <p id="top_list">1) Ilbagmanov (3 000 000$)<br>
                2) MR_BLT (3 000 000$<br>
                3) PRO100 (3 000 000$)<br>
                4) Killer2012 (3 000 000$)<br>
                5) Minecraft (3 000 000$)<br>
            </p>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
        crossorigin="anonymous"></script>
</body>
</html>
