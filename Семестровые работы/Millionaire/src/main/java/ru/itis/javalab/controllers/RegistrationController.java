package ru.itis.javalab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.javalab.dto.RegistrationFormDto;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.services.AccountService;

import javax.validation.Valid;
import java.util.UUID;

@Controller
public class RegistrationController {

    @Autowired
    AccountService accountService;

    @GetMapping(value = "/registration")
    public String doGet(Model model) {
        model.addAttribute("registrationFormDto", new RegistrationFormDto());
        return "registration";
    }

    @PostMapping(value = "/registration")
    public String doPost(@Valid RegistrationFormDto dto, BindingResult bindingResult, Model model) {
        if (!bindingResult.hasErrors()){
            Account account = Account.builder()
                    .username(dto.getUsername())
                    .email(dto.getEmail())
                    .password(dto.getPassword())
                    .uuid(UUID.randomUUID().toString())
                    .build();
            if(accountService.signUp(account)){
                return "redirect:/login";
            }
        }
        model.addAttribute("registrationFormDto", dto);
        return "registration";

    }
}
