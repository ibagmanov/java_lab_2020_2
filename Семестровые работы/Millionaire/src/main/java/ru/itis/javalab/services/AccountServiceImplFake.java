package ru.itis.javalab.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.javalab.models.Account;
import ru.itis.javalab.repositories.AccountRepository;
import ru.itis.javalab.utils.EmailUtil;
import ru.itis.javalab.utils.MailsGenerator;

@Service
@Profile("dev")
public class AccountServiceImplFake implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private MailsGenerator mailsGenerator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private EmailUtil emailUtil;

    @Value("${server.url}")
    private String serverUrl;

    @Value("${spring.mail.username}")
    private String from;

    @Override
    public boolean signUp(Account account) {
        try {
            account.setPassword(passwordEncoder.encode(account.getPassword()));
            account.setRole(Account.Role.USER);
            account.setState(Account.State.ACTIVE);
            accountRepository.save(account);
            String confirmMail = mailsGenerator.getMailForConfirm(serverUrl, account.getUuid());
            System.out.println(confirmMail);
            System.out.println("_______FAKE______");
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }
}

