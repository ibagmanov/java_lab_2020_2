package ru.itis.javalab.dto;

import lombok.Data;
import ru.itis.javalab.validations.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class RegistrationFormDto {
    private String username;
    @NotEmpty(message = "{errors.empty.email}")
    @Email(message = "{errors.incorrect.email}")
    private String email;
    @ValidPassword(message = "{errors.invalid.password}")
    private String password;
}
