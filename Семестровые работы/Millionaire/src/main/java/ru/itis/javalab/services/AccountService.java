package ru.itis.javalab.services;

import ru.itis.javalab.models.Account;

public interface AccountService {
    boolean signUp(Account account);
}
